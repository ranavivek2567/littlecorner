/**
 * Created by sandhuharjodh2561 on 7/27/2017.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { UsersModule } from './components/users/users.module';

import { routes } from './components/users/users.routes'

// Shared Components
import { SidebarMenuComponent } from './shared/sidebarMenu/sidebar-menu.component';
import { HeaderComponent } from './shared/header/header.component';

import { AdminComponent } from './adminComponent/admin.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';

const AdminRoutes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: '', component: DashboardComponent},
      { path: 'dashboard', component: DashboardComponent},
      { path: 'users', children: routes}
    ]
  }
]

const AdminLoginRoute: Routes = [
  {
    path: 'admin/login',
    component: LoginComponent
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    UsersModule,
    RouterModule.forChild( AdminRoutes ),
    RouterModule.forChild( AdminLoginRoute )
  ],
  exports: [
    RouterModule
  ],
  declarations: [
    AdminComponent,
    LoginComponent,
    SidebarMenuComponent,
    DashboardComponent,
    HeaderComponent
  ],
  providers: [
  ]
})

export class AdminModule {}
