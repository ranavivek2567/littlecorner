/**
 * Created by sandhuharjodh2561 on 8/2/2017.
 */
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: [
      './admin.css',
      '../sharedResources/style/animate.css',
      //'../sharedResources/style/bootstrap.css',
      '../sharedResources/font-awesome/css/font-awesome.css'
  ],
  encapsulation: ViewEncapsulation.None
})

export class AdminComponent {}
