import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';

import { UserByIdInterface } from '../graphql/schema';
import { GetUserDetailQuery } from '../graphql/queries';
import { UpdateUserMutation } from '../graphql/mutations';


@Component({
  selector: 'edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent {
 form: FormGroup;
  private sub: Subscription;
  public id;
  public user: any;

  constructor(
  formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apollo: Apollo
  ) {
    this.form = formBuilder.group({
      name: ['', [
        Validators.required,
      ]],
      email: ['']
    });
    this.apollo = apollo;
  }

  public ngOnInit(): void {
    const that = this
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.apollo.watchQuery<UserByIdInterface>({
      query: GetUserDetailQuery,
      variables: { 'id': this.id }
    }).subscribe(({ data }) => {
      that.user = data.user;
       this.form.setValue({name: data.user.name, email: data.user.email});
    });
  }

  public save() {
    if (!this.form.valid)
      return;
    this.apollo.mutate({
      mutation: UpdateUserMutation,
      variables: {
        "id": this.user.id,
        "data": {
          "name": this.form.value.name,
          "email": this.form.value.email
        }
      },
    })
      .take(1)
      .subscribe({
        next: ({ data }) => {
          console.log('edit user', data);
          // get edit data
          this.router.navigate(['/users']);
        }, error: (errors) => {
          console.log('there was an error sending the query', errors);
        }
      });
  }
}
