import gql from 'graphql-tag';

export const RemoveUserMutation = gql`
    mutation removeUser($id: ID!) {
        removeUser(id: $id) {
            id
            name
        }
    }
`;

export const UpdateUserMutation = gql`
    mutation updateUser($id: ID!, $data: UserInput) { 
        updateUser(id: $id, data: $data) { 
            id  
            name 
            email
        }
    }
`;

export const AddUserMutation = gql`
    mutation addUser($data: UserInput!) {
    addUser(data: $data)
    }
`;
