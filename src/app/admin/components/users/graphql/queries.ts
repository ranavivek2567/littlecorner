import gql from 'graphql-tag';

export const GetUserDetailQuery= gql`
     query GetUserDetailQuery($id: ID!) {
        user(id: $id) {
            id
            name
            email
        }
    }
`;

export const GetUsersQuery = gql`
  query Users {
    users {
        id
        name
        email
    }
  }
`;
