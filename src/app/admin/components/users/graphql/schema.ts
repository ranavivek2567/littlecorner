export interface UpdateUserInterface {
  updateUser: {
    id: string,
    name: string | null,
    email: string
  }
}

export interface DeleteUserInterface {
  removeUser: {
    id: string,
    name: string | null
  }
}

export interface UsersInterface {
  users: Array<{
    name: string | null,
    email: string | null
  }> | null;
}

export interface UserByIdInterface {
    user: {
      id: string,
      name: string | null,
      email: string | null
  }
}
