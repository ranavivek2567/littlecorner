import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../users.service';

@Component({
  selector: 'new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss'],
  providers: [UsersService]
})
export class NewUserComponent {
  private form: FormGroup;

  constructor(private _userService: UsersService, formBuilder: FormBuilder) {
    this.form = formBuilder.group({
        name: ['', [
          Validators.required,
        ]],
        email: ['', [
            Validators.email
        ]]
      });
  }
  public save() {
    if (!this.form.valid) return;
    let userdata = {
      'name' : this.form.value.name,
      'email': this.form.value.email
    };
    console.log('asdas',userdata);
    this._userService.addUser(userdata);
  }
}
