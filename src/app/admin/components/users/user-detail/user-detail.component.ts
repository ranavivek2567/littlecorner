import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { Subject } from 'rxjs/Subject';
import { DocumentNode } from 'graphql';
import { client } from '../graphql.client';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';


import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { UserByIdInterface } from '../graphql/schema';
import { GetUserDetailQuery } from '../graphql/queries';


@Component({
    templateUrl: './user-detail.component.html',
      styleUrls: ['./user-detail.component.scss']

})

export class userDetailComponent implements OnInit, OnDestroy {
    public pageTitle: string = 'User detail:';
    public user: any;
    public errorMessage: string;
    private apollo: Apollo;
    public userControl = new FormControl();
    // Observable variable of the graphql query
    public nameFilter: Subject<string> = new Subject<string>();
    private sub: Subscription;
    public id;
    // Inject Angular2Apollo service
    constructor(apollo: Apollo, private route: ActivatedRoute) {
        this.apollo = apollo;
    }

    public ngOnInit(): void {
        this.sub = this.route.params.subscribe(params => {
            this.id = params['id'];
        });
        this.apollo.watchQuery<UserByIdInterface>({
            query: GetUserDetailQuery,
            variables: { "id": this.id }
        }).subscribe(({ data }) => {
            this.user = data.user;
        });
    }
    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
