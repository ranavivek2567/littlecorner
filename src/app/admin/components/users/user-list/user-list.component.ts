import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Apollo, ApolloQueryObservable } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { Subject } from 'rxjs/Subject';
import { DocumentNode } from 'graphql';
import { UsersService } from '../users.service';
import { MdSnackBar } from '@angular/material';


import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import { UsersInterface } from '../graphql/schema';
@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  public users = Array<object>;
  public listUserFilter: string;
  public userControl = new FormControl();
  private nameFilter: Subject<string> = new Subject<string>();

  // Inject Angular2Apollo service
  constructor(private _userService: UsersService, public snackBar: MdSnackBar) {
  }

  public ngOnInit() {
    this.users = this._userService.get();
    // Add debounce time to wait 300 ms for a new change instead of keep hitting the server
    this.userControl.valueChanges.debounceTime(300).subscribe(name => {
      this.nameFilter.next(name);
    });
  }
  public deleteUser(id: string) {
    this._userService.delete(id)
      .then((response) => {
        this.openSnackBar(response.message, 'Delete');
        //this.users.refetch();
      })
      .catch((error) => {
        this.openSnackBar(error.message, 'error');
      };
  }
  public openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }
}
