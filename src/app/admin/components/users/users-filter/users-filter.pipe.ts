import {PipeTransform,Pipe} from '@angular/core';
import {IUser} from '../user.interface'
@Pipe({
    name:'usersFilter'
})
export class UsersFilterPipe implements PipeTransform{
transform(value :IUser[], filtetBy:string):IUser[]{
    filtetBy= filtetBy? filtetBy.toLocaleLowerCase():null;//search in each product if is the same
    return filtetBy ? value.filter((user:IUser)=>user.name.toLocaleLowerCase().indexOf(filtetBy) !==-1) : value;
}

}
