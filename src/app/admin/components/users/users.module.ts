import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule }  from '@angular/http';
import { MaterialModule } from '@angular/material';

import { UserListComponent } from './user-list/user-list.component';
import { userDetailComponent } from './user-detail/user-detail.component';
import { NewUserComponent } from './new-user/new-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UsersFilterPipe} from './users-filter/users-filter.pipe';

import  { UsersService } from './users.service'

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    UserListComponent,
    NewUserComponent,
    EditUserComponent,
    UsersFilterPipe,
    userDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpModule,
    MaterialModule,
    // ApolloModule.forRoot(client)
  ],
  providers: [
    UsersService
  ]
})
export class UsersModule {}
