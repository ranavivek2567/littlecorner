import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { userDetailComponent } from './user-detail/user-detail.component';


export const routes = [
    {
        path: '', children: [
            { path: 'user-detail/:id', component: userDetailComponent },
            { path: 'new', component: NewUserComponent },
            { path: 'edit/:id', component: EditUserComponent },
            { path: '', component: UserListComponent }
        ]
    },
];
