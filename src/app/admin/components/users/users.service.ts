import { Injectable } from '@angular/core';
import { IUser } from './user.interface';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Constants } from '../../sharedResources/constant';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

@Injectable()
export class UsersService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private usersUrl = Constants.apiUrl + "/users";
    constructor(private http: Http) {

    };

    public get() {
        return this.http.get(this.usersUrl, { headers: this.headers })
            .toPromise()
            .then((res) => res.json());
    }

    public addUser(data) {
        return this.http.post(this.usersUrl,
         { name : data.name, email: data.email }, { headers: this.headers })
         .toPromise()
         .then((res) => res.json());
    }

    public delete(id) {
        console.log('id',id);
        return this.http.delete(this.usersUrl + '/:' + id)
            .toPromise()
            .then((res) => res.json());
    }
}
